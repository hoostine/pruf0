var sampleData = [
    {
		"_id" : "67FAF",
		"name" : "Akeem Best",
		"class" : "lab"
	}, {
		"_id" : "95AEC",
		"name" : "Brent Banks",
		"class" : "distributor"
	}, {
		"_id" : "4A771",
		"name" : "Christine Baird",
		"class" : "lab"
	}, {
		"_id" : "8CCFA",
		"name" : "Tatyana Mcneil",
		"class" : "distributor"
	}, {
		"_id" : "05FC7",
		"name" : "Blythe Blair",
		"class" : "rep"
	}, {
		"_id" : "9FA97",
		"name" : "Kennedy Harrington",
		"class" : "rep"
	}, {
		"_id" : "0773D",
		"name" : "Flynn Pratt",
		"class" : "distributor"
	}, {
		"_id" : "8F524",
		"name" : "Lars Morris",
		"class" : "distributor"
	}, {
		"_id" : "15FEE",
		"name" : "Oleg Owens",
		"class" : "lab"
	}, {
		"_id" : "A6C61",
		"name" : "Garrett Brewer",
		"class" : "rep"
	}, {
		"_id" : "0C6C5",
		"name" : "Olivia Robbins",
		"class" : "lab"
	}, {
		"_id" : "3B6B9",
		"name" : "Maya Little",
		"class" : "distributor"
	}, {
		"_id" : "7FF19",
		"name" : "Cheryl Vega",
		"class" : "rep"
	}, {
		"_id" : "F7154",
		"name" : "Veronica Morris",
		"class" : "distributor"
	}, {
		"_id" : "68589",
		"name" : "Paloma Underwood",
		"class" : "distributor"
	}, {
		"_id" : "F077D",
		"name" : "Elaine Christian",
		"class" : "rep"
	}, {
		"_id" : "4E06F",
		"name" : "Jerome Jones",
		"class" : "distributor"
	}, {
		"_id" : "83D82",
		"name" : "Ocean Mcgee",
		"class" : "rep"
	}, {
		"_id" : "C5E0C",
		"name" : "Finn Ramos",
		"class" : "distributor"
	}, {
		"_id" : "062A9",
		"name" : "Brianna Workman",
		"class" : "distributor"
	}, {
		"_id" : "7DD39",
		"name" : "Chiquita Britt",
		"class" : "distributor"
	}, {
		"_id" : "8777E",
		"name" : "Maxwell Dalton",
		"class" : "lab"
	}, {
		"_id" : "C3E26",
		"name" : "Halee Johnson",
		"class" : "lab"
	}, {
		"_id" : "89719",
		"name" : "Barclay Beard",
		"class" : "distributor"
	}, {
		"_id" : "771F9",
		"name" : "Steven Pruitt",
		"class" : "rep"
	}, {
		"_id" : "0A1A8",
		"name" : "Colleen May",
		"class" : "rep"
	}, {
		"_id" : "EC9C6",
		"name" : "Ursa Madden",
		"class" : "lab"
	}, {
		"_id" : "44B02",
		"name" : "Hedda Nguyen",
		"class" : "rep"
	}, {
		"_id" : "69B43",
		"name" : "Daniel Cruz",
		"class" : "lab"
	}, {
		"_id" : "69084",
		"name" : "Pearl Sharp",
		"class" : "distributor"
	}, {
		"_id" : "01F49",
		"name" : "Duncan Bowman",
		"class" : "rep"
	}, {
		"_id" : "CA5CC",
		"name" : "Guy Kennedy",
		"class" : "rep"
	}, {
		"_id" : "62FF2",
		"name" : "Stone Cherry",
		"class" : "rep"
	}, {
		"_id" : "D8C97",
		"name" : "Reuben Casey",
		"class" : "lab"
	}, {
		"_id" : "A3C20",
		"name" : "Yasir Knapp",
		"class" : "lab"
	}, {
		"_id" : "89032",
		"name" : "Alvin Armstrong",
		"class" : "rep"
	}, {
		"_id" : "44506",
		"name" : "Gretchen Paul",
		"class" : "rep"
	}, {
		"_id" : "85E00",
		"name" : "Lavinia Clarke",
		"class" : "distributor"
	}, {
		"_id" : "6BD9E",
		"name" : "Ainsley Odonnell",
		"class" : "rep"
	}, {
		"_id" : "4FC94",
		"name" : "Ralph Poole",
		"class" : "lab"
	}, {
		"_id" : "8FE42",
		"name" : "Vance Hawkins",
		"class" : "rep"
	}, {
		"_id" : "2970D",
		"name" : "Camden Kinney",
		"class" : "lab"
	}, {
		"_id" : "713ED",
		"name" : "Noble Mcleod",
		"class" : "lab"
	}, {
		"_id" : "503E0",
		"name" : "Lucius Holmes",
		"class" : "lab"
	}, {
		"_id" : "A7264",
		"name" : "Samson Underwood",
		"class" : "lab"
	}, {
		"_id" : "6AAD4",
		"name" : "Duncan Harris",
		"class" : "distributor"
	}, {
		"_id" : "2FD3C",
		"name" : "Drew Alston",
		"class" : "lab"
	}, {
		"_id" : "DFDB0",
		"name" : "Rebecca Castro",
		"class" : "lab"
	}, {
		"_id" : "08658",
		"name" : "Irma Moran",
		"class" : "rep"
	}, {
		"_id" : "28284",
		"name" : "Yolanda Frank",
		"class" : "distributor"
	}, {
		"_id" : "40F58",
		"name" : "Alika Melendez",
		"class" : "rep"
	}, {
		"_id" : "D4FAC",
		"name" : "Mikayla Fuller",
		"class" : "rep"
	}, {
		"_id" : "614E0",
		"name" : "Brent Lowe",
		"class" : "rep"
	}, {
		"_id" : "CE869",
		"name" : "Amaya Aguirre",
		"class" : "lab"
	}, {
		"_id" : "48D8E",
		"name" : "August Barber",
		"class" : "distributor"
	}, {
		"_id" : "FE7CC",
		"name" : "Kai Douglas",
		"class" : "rep"
	}, {
		"_id" : "C1BC5",
		"name" : "Evangeline Bush",
		"class" : "lab"
	}, {
		"_id" : "AAC32",
		"name" : "Eaton Sanders",
		"class" : "rep"
	}, {
		"_id" : "27D9D",
		"name" : "Ulric Perez",
		"class" : "rep"
	}, {
		"_id" : "E92DE",
		"name" : "Kieran Morrison",
		"class" : "distributor"
	}, {
		"_id" : "75AD3",
		"name" : "Flynn Collins",
		"class" : "rep"
	}, {
		"_id" : "A166F",
		"name" : "Brian Wilson",
		"class" : "rep"
	}, {
		"_id" : "C1864",
		"name" : "Victor Cunningham",
		"class" : "lab"
	}, {
		"_id" : "63471",
		"name" : "Emma Warner",
		"class" : "distributor"
	}, {
		"_id" : "06326",
		"name" : "Ulla Nunez",
		"class" : "rep"
	}, {
		"_id" : "9BFC5",
		"name" : "Haviva Swanson",
		"class" : "distributor"
	}, {
		"_id" : "A99F0",
		"name" : "Nigel Allen",
		"class" : "rep"
	}, {
		"_id" : "D8551",
		"name" : "Brittany Hendricks",
		"class" : "rep"
	}, {
		"_id" : "A86C8",
		"name" : "Hadassah Murphy",
		"class" : "lab"
	}, {
		"_id" : "D8C4D",
		"name" : "Ina Thomas",
		"class" : "lab"
	}, {
		"_id" : "86E4E",
		"name" : "Benjamin Richard",
		"class" : "lab"
	}, {
		"_id" : "F702B",
		"name" : "Nasim Hayes",
		"class" : "lab"
	}, {
		"_id" : "3F306",
		"name" : "Alisa Short",
		"class" : "distributor"
	}, {
		"_id" : "F0461",
		"name" : "Diana Johns",
		"class" : "distributor"
	}, {
		"_id" : "B9190",
		"name" : "Idola Reese",
		"class" : "rep"
	}, {
		"_id" : "EFFD7",
		"name" : "Eden Rosales",
		"class" : "lab"
	}, {
		"_id" : "44B3B",
		"name" : "Basil Battle",
		"class" : "rep"
	}, {
		"_id" : "74A3A",
		"name" : "Ivana Winters",
		"class" : "distributor"
	}, {
		"_id" : "61760",
		"name" : "Erasmus Brady",
		"class" : "lab"
	}, {
		"_id" : "A4862",
		"name" : "Kirsten Wooten",
		"class" : "lab"
	}, {
		"_id" : "7731C",
		"name" : "Lewis Hewitt",
		"class" : "lab"
	}, {
		"_id" : "9F359",
		"name" : "Hayes Johnson",
		"class" : "rep"
	}, {
		"_id" : "018AB",
		"name" : "Jane Bates",
		"class" : "distributor"
	}, {
		"_id" : "F6E1D",
		"name" : "Ashely Cote",
		"class" : "distributor"
	}, {
		"_id" : "211BE",
		"name" : "Brynne Hooper",
		"class" : "rep"
	}, {
		"_id" : "0CABC",
		"name" : "Diana Oneil",
		"class" : "distributor"
	}, {
		"_id" : "584C5",
		"name" : "Mark Barr",
		"class" : "rep"
	}, {
		"_id" : "03559",
		"name" : "Maisie Mays",
		"class" : "distributor"
	}, {
		"_id" : "12E74",
		"name" : "Abdul Henson",
		"class" : "rep"
	}, {
		"_id" : "E9F4B",
		"name" : "Allegra Contreras",
		"class" : "lab"
	}, {
		"_id" : "2AE79",
		"name" : "Caesar Kramer",
		"class" : "distributor"
	}, {
		"_id" : "A989D",
		"name" : "Kiona Rojas",
		"class" : "rep"
	}, {
		"_id" : "A18C8",
		"name" : "Lynn Mathews",
		"class" : "lab"
	}, {
		"_id" : "27B70",
		"name" : "Leo Frost",
		"class" : "distributor"
	}, {
		"_id" : "73610",
		"name" : "Jessica Acosta",
		"class" : "rep"
	}, {
		"_id" : "6A01F",
		"name" : "Summer Murray",
		"class" : "rep"
	}, {
		"_id" : "9B123",
		"name" : "Yuri Sampson",
		"class" : "distributor"
	}, {
		"_id" : "7C5AE",
		"name" : "Tyrone Ray",
		"class" : "lab"
	}, {
		"_id" : "0E4A8",
		"name" : "Jasmine Estrada",
		"class" : "lab"
	}, {
		"_id" : "71FBF",
		"name" : "Emmanuel Rodgers",
		"class" : "lab"
	}
];

sampleReps = [];
sampleLabs = [];
sampleDistributors = [];

sampleData.forEach(function (sample) {
    switch (sample.class) {
        case 'lab':
            sampleLabs.push(sample);
            break;
        case 'distributor':
            sampleDistributors.push(sample);
            break;
        case 'rep':
            sampleReps.push(sample);
            break;
    }
});
